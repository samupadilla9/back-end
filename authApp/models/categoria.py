from django.db import models

class Categoria(models.Model):
    catIdCategoria=models.AutoField(primary_key=True)    
    catNomCategoria=models.CharField(max_length = 256)    