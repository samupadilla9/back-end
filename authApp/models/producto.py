from django.db import models
from .categoria import Categoria

class Producto(models.Model):
    proIdProducto=models.AutoField(primary_key=True)    
    #proIdCategoria=models.ForeignKey(Categoria,on_delete=models.CASCADE)          
    proIdCategoria=models.IntegerField()
    proDescripcion=models.CharField(max_length = 256)
    proStock=models.IntegerField()
    proPrecioCompra=models.DecimalField(max_digits=15, decimal_places=2)
    proPrecVenta=models.DecimalField(max_digits=15, decimal_places=2)
    
