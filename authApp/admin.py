from django.contrib import admin
from .models.account import Account
from .models.user import User
from .models.producto import Producto
from .models.categoria import Categoria
# Register your models here.

admin.site.register(User)
admin.site.register(Account)
admin.site.register(Producto)
admin.site.register(Categoria)