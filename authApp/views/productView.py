from rest_framework import status,views
#from rest_framework.decorators import api_view
from rest_framework.response import Response
from authApp.models.producto import Producto
from authApp.serializers.productSerializer import ProductSerializer

#@api_view(['GET', 'POST'])
class ProductView(views.APIView):
    """
    def product_list(request):
        
        List all code Product, or create a new Product.
        """
    def get(self, request, *args, **kwargs):
        if request.method == 'GET':
            productos = Producto.objects.all()
            serializer = ProductSerializer(productos, many=True)
            return Response(serializer.data)

    def post(self, request, *args, **kwargs):
        if request.method == 'POST':
            serializer = ProductSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)