from authApp.models.categoria import Categoria
from rest_framework import serializers

class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Categoria
        fields = ['catIdCategoria', 'catNomCategoria']