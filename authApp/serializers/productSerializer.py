from authApp.models.producto import Producto
from rest_framework import serializers

class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Producto
        fields = ['proIdProducto', 'proIdCategoria', 'proDescripcion','proStock','proPrecioCompra','proPrecVenta']